# ec2-with-docker



## Getting started

This project will deploy a new structure with the network resources, an EC2 instance after that will will install the docker and finally deploy a go application in docker. 


## Which platforms you can deploy?

platforms:
- Debian
    versions:
    - buster
    - bullseye
- Ubuntu
    versions:
    - focal
    - jammy

## Executing locally

### Dendencies

In the machine or container that you are gonna execute you must have:

- terraform = v1.3.3
- ansible = 2.9

You need an AWS account and generate:

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

Here is the environment variables that you need to set:

- AWS_ACCESS_KEY_ID (AWS_ACCESS_KEY_ID)
- AWS_SECRET_ACCESS_KEY (AWS_SECRET_ACCESS_KEY)
- TF_VAR_key_name (This is the name of key pair)

Example:
```
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
export TF_VAR_key_name="<key pair name>"
```

Step 1 - clone this repo and the repo that installs the docker using ansible-playbook:

```
git clone https://gitlab.com/challenge-sre-cloud/ec2-with-docker.git
cd ec2-with-docker
git clone https://gitlab.com/challenge-sre-cloud/ansible-install-docker.git
```

Step 2 - execute terraform init and terraform validate

```
terraform init
terraform validate
```

Expected output:

```
/ec2-with-docker # terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Finding latest version of hashicorp/tls...
- Finding latest version of hashicorp/local...
- Installing hashicorp/aws v4.36.1...
- Installed hashicorp/aws v4.36.1 (signed by HashiCorp)
- Installing hashicorp/tls v4.0.3...
- Installed hashicorp/tls v4.0.3 (signed by HashiCorp)
- Installing hashicorp/local v2.2.3...
- Installed hashicorp/local v2.2.3 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

/ec2-with-docker # terraform validate
Success! The configuration is valid.
```


Step 3 - execute terraform plan

```
terraform plan -var-file="variables.tfvars" -out=challenge
```

End of example output:

```
  # tls_private_key.private-key-sre will be created
  + resource "tls_private_key" "private-key-sre" {
      + algorithm                     = "RSA"
      + ecdsa_curve                   = "P224"
      + id                            = (known after apply)
      + private_key_openssh           = (sensitive value)
      + private_key_pem               = (sensitive value)
      + private_key_pem_pkcs8         = (sensitive value)
      + public_key_fingerprint_md5    = (known after apply)
      + public_key_fingerprint_sha256 = (known after apply)
      + public_key_openssh            = (known after apply)
      + public_key_pem                = (known after apply)
      + rsa_bits                      = 4096
    }

Plan: 10 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + ip_address  = (known after apply)
  + private_key = (sensitive value)
  + public_dns  = (known after apply)

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Saved the plan to: challenge

To perform exactly these actions, run the following command to apply:
    terraform apply "challenge"
```

Step 4 - apply the terraform

```
terraform apply "challenge"
```

End of example output:

```
aws_instance.sre (remote-exec): 73cb4d56b356: Pull complete
aws_instance.sre (remote-exec): Digest: sha256:fbcd3a1af9835c26449694b39f95aecce0a49cacdfc2a088849eb21511bad94a
aws_instance.sre (remote-exec): Status: Downloaded newer image for bmendesl/golang-app:1.0.3
aws_instance.sre (remote-exec): 8fad97e062c1bfd965d9fd54f7602f068d984c09d2267abdfe033ae9df97fe12
aws_instance.sre (remote-exec): Hey! You win 1 point
aws_instance.sre: Still creating... [3m30s elapsed]
aws_instance.sre: Creation complete after 3m31s [id=i-0c18329c1685195ec]

Apply complete! Resources: 10 added, 0 changed, 0 destroyed.

Outputs:

ip_address = "3.91.90.73"
private_key = <sensitive>
public_dns = "ec2-3-91-90-73.compute-1.amazonaws.com"
```

The step below is only you want to destroy all that was created.

Step 5 - You can test if the application is working. Copy the public_dns from the last execution. In this example the is **ec2-3-91-90-73.compute-1.amazonaws.com**

```
curl http://ec2-3-91-90-73.compute-1.amazonaws.com
```

Example output
```
/ec2-with-docker # curl http://ec2-3-91-90-73.compute-1.amazonaws.com
Hey! You win 7 points
```

Step 6 - terraform destroy

```
terraform destroy -auto-approve -var-file="variables.tfvars"
```

## Did you fork this project?

If you forked this project for your own use, please go to your project's Settings and remove the forking relationship, which won't be necessary unless you want to contribute back to the upstream project.

Do not forget to set the variables:

- AWS_ACCESS_KEY_ID (aws access id)
- AWS_SECRET_ACCESS_KEY (aws secret key )
- key_name (name of the key pair) 
- PHASE (BUILD or DESTROY)

at you project's Settings > CI/CD > Variables.

## Support
If you need any help with this code send me an email: bruno@bruno.com.br

## Roadmap
To do:

- Stage execution segregating by the branches
- Remove the hard code variables in the terraform project
- Add the option of input a existent key pair
- Implement the terraform scan, to check the security of the code
- Improve this Readme
- Segregate the network and ec2 creation into modules
- Remove the test of the application from the terraform to the pipeline stage
- Add the tfstate into a s3 bucket
- Add option to destroy with exceptions

## Contributing
If you want to contribute, you can open an issue or if you prefer be more activelly you can fork this project and make a pull request to a new branch into the project.
