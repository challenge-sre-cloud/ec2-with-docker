##################################################################################
# DATA
##################################################################################

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"] # Ubuntu
}

# resource "aws_key_pair" "sre-cloud" {
#   key_name   = var.key_name
#   public_key = var.PUBLIC_KEY
# }

resource "tls_private_key" "private-key-sre" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.private-key-sre.public_key_openssh
}

resource "local_sensitive_file" "sre-key" {
    content  = tls_private_key.private-key-sre.private_key_pem
    filename = "${path.module}/${aws_key_pair.generated_key.key_name}"
    file_permission = 0400
}

resource "aws_instance" "sre" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.subnetsre.id
  vpc_security_group_ids = [aws_security_group.sre-sg.id]
  # key_name      = aws_key_pair.sre-cloud.key_name
  key_name      = aws_key_pair.generated_key.key_name

  provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo apt install python3 -y", "echo Done! > /tmp/teste"]

    connection {
      host        = aws_instance.sre.public_ip
      type        = "ssh"
      user        = "ubuntu"
      # private_key = file("${aws_key_pair.sre-cloud.key_name}")
      private_key = tls_private_key.private-key-sre.private_key_pem
    }
  }
  
  provisioner "local-exec" {
    command = <<-EOT
    cp -rf ansible-install-docker/docker/ .
    cp -rf ansible-install-docker/install-docker.yml .
    sed -i 's/hosts: localhost/hosts: all/g' install-docker.yml
    ls -latr
    sleep 5
    EOT
    interpreter = ["sh", "-c"]
  }

  provisioner "local-exec" {
    command = "sleep 60;ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i '${aws_instance.sre.public_ip},' --private-key=${aws_key_pair.generated_key.key_name} install-docker.yml"
    # command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i '${aws_instance.sre.public_ip},' --private-key=${aws_key_pair.sre-cloud.key_name} install-docker.yml"
  }

  provisioner "remote-exec" {
    inline = ["sudo docker run -dti -p 80:8080 --restart always bmendesl/golang-app:1.0.3","sleep 4", "curl http://127.0.0.1"]

    connection {
      host        = aws_instance.sre.public_ip
      type        = "ssh"
      user        = "ubuntu"
      # private_key = file("${aws_key_pair.sre-cloud.key_name}")
      private_key = tls_private_key.private-key-sre.private_key_pem
    }
  }

  tags = {
    Name = var.machine_tag_name
  }
}