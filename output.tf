output "ip_address" {
  value = aws_instance.sre.public_ip
}

output "public_dns" {
  value = aws_instance.sre.public_dns
}

output "private_key" {
  value     = tls_private_key.private-key-sre.private_key_pem
  sensitive = true
}