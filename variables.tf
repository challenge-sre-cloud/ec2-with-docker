variable "aws_region" {
  type        = string
  description = "Region for AWS Resources"
  default     = "us-east-1"
}

# variable "PUBLIC_KEY" {
#   type        = string
#   description = "AWS public key to access the ec2 machine"
#   sensitive   = true
# }

variable "key_name" {
  type        = string
  description = "AWS public key to access the ec2 machine"
  sensitive   = true
}

variable "machine_tag_name" {
  type        = string
  description = "Tag for the name of machime"
  default     = ""
}

variable "instance_type" {
  type        = string
  description = "Type for EC2 Instnace"
  default     = "t2.medium"
}

variable "cidr_blocks_ips" {
  type        = list
  description = "IPs address that you need do ssh into the ec2 machine"
  default = ["0.0.0.0/0"]
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "Enable DNS hostnames in VPC"
  default     = true
}

variable "vpc_cidr_block" {
  type        = string
  description = "Base CIDR Block for VPC"
  default     = "10.0.0.0/16"
}

variable "vpc_subnetsre_cidr_block" {
  type        = string
  description = "CIDR Block for Subnet 1 in VPC"
  default     = "10.0.0.0/24"
}

variable "map_public_ip_on_launch" {
  type        = bool
  description = "Map a public IP address for Subnet instances"
  default     = true
}

variable "company" {
  type        = string
  description = "Company name for resource tagging"
  default     = "Challenge Company"
}

variable "project" {
  type        = string
  description = "Project name for resource tagging"
}

variable "billing_code" {
  type        = string
  description = "Billing code for resource tagging"
}